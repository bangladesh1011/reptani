package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class Base {
	public static WebDriver driver;//eita alwa static hobe,coz we call it.
	static FileInputStream fis;
	public static Properties LocatorProp;  //LocatorProp
	public static Properties configProp;  //1
	//String projectPath=System.getProperty("user.dir");//it returns project path
	
	@BeforeSuite
	public void setup() throws IOException {
		
		//project directory path
		String projectPath=System.getProperty("user.dir");//it returns project path
		
		fis=new FileInputStream(projectPath + "\\src\\properties\\locator.properties"); // c://java//workspace//airBNB//src//properties//locators.properties
		//locator properties;
		LocatorProp=new Properties();
		LocatorProp.load(fis);
		
		//config properties
		fis=new FileInputStream(projectPath+"\\src\\properties\\config.properties");
		configProp=new Properties();
		configProp.load(fis);
		
		if(configProp.getProperty("browser").contains("firefox")) {
			System.setProperty("webdriver.gecko.driver", projectPath+"\\src\\executables\\geckodriver.exe");
			driver= new FirefoxDriver();
			
		}//if
		else if(configProp.getProperty("browser").contains("chrome")) {
			System.setProperty("webdriver.chrome.driver", projectPath+"\\src\\executables\\chromedriver.exe");
			driver= new ChromeDriver();
			
		}//if
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
	}
	public void navigateUrl(String url) {
		driver.get(url);
	}
	public void sendKeys(By by,String value) {
		driver.findElement(by).sendKeys(value);
	}
	public void click(By by) {
	
		driver.findElement(by).click();
	}
	public String getText(By by) {
		return driver.findElement(by).getText();
	}
	//@AfterSuite
	//public void close() {
	//	driver.close();
	//}
}
